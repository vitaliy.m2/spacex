import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Zoom } from "../../assets/Zoom.svg";

import styles from './Input.module.css';

const Input = ({
  onChange,
  search,
  ...rest
}) => {
  return (
    <div className={styles.inputWrapper}>
      {search && <Zoom />}
      <input
        onChange={onChange}
        {...rest}
      />
    </div>
  );
};

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  search: PropTypes.bool.isRequired,
};

Input.defaultProps = {
  search: false
};
export default Input;
