import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useParams } from 'react-router';
import { Input } from 'components';
import { validateNumber, calcRequiredCargoBays } from './ShipmentDetails.utils';

import styles from './ShipmentDetails.module.css';

const ShipmentDetails = ({ shipments }) => {
  const { shipmentId } = useParams();
  const [shipment, setShipment] = useState();
  const [boxes, setBoxes] = useState("");

  useEffect(() => {
    if (!shipmentId) {
      return
    }
    const currentShipment = shipments.find(shipment => shipment.id === shipmentId);
    setShipment(currentShipment);
    setBoxes(currentShipment.boxes);
  }, [shipmentId, shipments]);

  const cargoBaysCount = useMemo(() => calcRequiredCargoBays(boxes), [boxes]);

  const onBoxesChange = (e) => {
    const value = e.target.value;
    const isValueValid = value.split(',').every(validateNumber);
    if (isValueValid) {
      setBoxes(e.target.value);
    }
  };

  if (!shipment) {
    return (
      <div className={styles.shipment}>
        <p>Please, select shipment</p>
      </div>
    )
  }

  return (
    <div className={styles.shipment}>
      <h3 className={styles.shipmentTitle}>{shipment.name}</h3>
      <a href={`mailto:${shipment.email}`}>{shipment.email}</a>
      <label
        className={styles.cargoLabel}
        htmlFor="boxesInput"
      >
        Cargo boxes
      </label>
      <Input
        id="boxesInput"
        value={boxes}
        onChange={onBoxesChange}
      />
      <p className={styles.cargoBaysCountTitle}>Number of required cargo bays</p>
      <p className={styles.cargoBaysCount}>{cargoBaysCount}</p>
    </div>
  );
};

ShipmentDetails.propTypes = {
  shipments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    boxes: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired
  })).isRequired
};
export default React.memo(ShipmentDetails)
