const UNITS_PER_BAY = 10;

export const calcRequiredCargoBays = (boxes) => {
  if (!boxes) {
    return null;
  }
  const sum = boxes.split(',').reduce((a, b) => a + +b, 0);
  return Math.ceil(sum / UNITS_PER_BAY)
};

export const validateNumber = (v) => {
  const number = Number(v);
  return typeof number === "number" && !Number.isNaN(number);
};
