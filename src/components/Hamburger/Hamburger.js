import React from 'react';
import PropTypes from 'prop-types';
import CN from 'classnames';
import styles from './Hamburger.module.css';

const Hamburger = ({ onBurgerClick, isMobileOpen }) => {
  return (
    <div
      onClick={() => onBurgerClick(opened => !opened)}
      className={CN(styles.hamburger, {[styles.isOpened]: isMobileOpen})}
    >
      <span className={styles.line} />
      <span className={styles.line} />
      <span className={styles.line} />
    </div>
  );
};

Hamburger.propTypes = {
  onBurgerClick: PropTypes.func.isRequired,
  isMobileOpen: PropTypes.bool.isRequired
};

export default Hamburger;
