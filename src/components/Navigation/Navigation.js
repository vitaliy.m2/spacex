import React from 'react';
import PropTypes from 'prop-types';
import CN from 'classnames';
import { Link } from 'react-router-dom';

import styles from './Navigation.module.css';

const Navigation = ({ shipments, isMobileOpen, closeMobileMenu }) => {
  return (
    <nav
      className={CN(styles.nav, {[styles.mobileOpened]: isMobileOpen})}
    >
      <h3>Shipment List</h3>
      {shipments.length > 0 ? (
        <ul className={styles.shipmentsList}>
          {shipments.map(shipment => (
            <li onClick={closeMobileMenu} key={shipment.id}>
              <Link to={`/${shipment.id}`}>
                {shipment.name}
              </Link>
            </li>
          ))}
        </ul>
      ) : (
        <div>Shipments not found</div>
      )}
    </nav>
  );
};

Navigation.propTypes = {
  shipments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    boxes: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired
  })).isRequired,
  isMobileOpen: PropTypes.bool.isRequired,
  closeMobileMenu: PropTypes.func.isRequired
};

export default Navigation;
