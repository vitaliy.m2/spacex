import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Input, Hamburger } from 'components';
import { ReactComponent as Logo } from "../../assets/Logo.svg";

import styles from './Header.module.css';

const Header = ({ isMobileOpen, onBurgerClick, onSearch, searchValue }) => {
  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link to="/">
          <Logo />
        </Link>
      </div>
      <Hamburger
        onBurgerClick={onBurgerClick}
        isMobileOpen={isMobileOpen}
      />
      <div className={styles.search}>
        <Input
          placeholder="Search"
          onChange={onSearch}
          value={searchValue}
          search
        />
      </div>
    </header>
  );
};

Header.propTypes = {
  isMobileOpen: PropTypes.bool.isRequired,
  onBurgerClick: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  searchValue: PropTypes.string,
};

export default Header;
