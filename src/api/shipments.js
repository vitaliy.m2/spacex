export const getShipments = async () => {
  try {
    const response = await fetch('/shipments.json');
    return await response.json();
  } catch(e) {
    throw e
  }
};
