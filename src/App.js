import React, { useState } from 'react';
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";
import { HomePage } from 'pages';

import './App.css';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/:shipmentId" element={<HomePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
