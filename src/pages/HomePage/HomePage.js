import React, {useEffect, useRef, useState, useMemo } from 'react'
import {Header, ShipmentDetails, Navigation, Loader} from 'components';
import {getShipments} from 'api/shipments';

import styles from './HomePage.module.css';

const HomePage = () => {
  const shipmentsSource = useRef([]);
  const [searchValue, setSearchValue ] = useState('');
  const [shipments, setShipments] = useState([]);
  const [isMobileOpen, setIsMobileOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchShipments = async () => {
      const shipments = await getShipments();
      setShipments(shipments);
      shipmentsSource.current = shipments;
      setIsLoading(false);
    };
    setTimeout(fetchShipments, 1500);
  }, []);

  const onSearch = (e) => {
    const value = e.target.value;
    setSearchValue(value);
    const filteredShipments = shipmentsSource.current.filter(shipment => (
      shipment.name.toLowerCase().includes(value.toLowerCase())
    ));
    setShipments(filteredShipments)
  };

  const closeMobileMenu = () => {
    if (isMobileOpen) {
      setIsMobileOpen(false);
    }
  };

  if (isLoading) {
    return <Loader />
  }
  return (
    <div className={styles.app}>
      <Header
        searchValue={searchValue}
        onSearch={onSearch}
        onBurgerClick={setIsMobileOpen}
        isMobileOpen={isMobileOpen}
      />
      <main className={styles.main}>
        <Navigation
          shipments={shipments}
          closeMobileMenu={closeMobileMenu}
          isMobileOpen={isMobileOpen}
        />
        <ShipmentDetails
          shipments={shipmentsSource.current}
        />
      </main>
    </div>
  );
};

export default HomePage;
